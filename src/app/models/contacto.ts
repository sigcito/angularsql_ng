export interface Contacto{
    id?: number;
    nombre?: string;
    nombre_empresa?: string;
    email?: string;
    telefono?: string;
    categoria_id?: number;
    mensaje?: string;
    image?: string;
    created_at?: Date;
}
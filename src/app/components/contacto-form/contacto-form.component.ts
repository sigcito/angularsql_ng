import { Component, HostBinding, OnInit } from '@angular/core';
import { ContactoService } from '../../services/contacto.service';

import { Categoria } from 'src/app/models/categoria';
import { Contacto } from 'src/app/models/contacto';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-contacto-form',
  templateUrl: './contacto-form.component.html',
  styleUrls: ['./contacto-form.component.css']
})
export class ContactoFormComponent implements OnInit {

  categoria: any = [];

  selectedCategory: number = 0;

  @HostBinding('class') classes = 'row';


  contacto: Contacto = {
    id: 0,
    nombre: '',
    nombre_empresa: '',
    email: '',
    telefono: '',
    categoria_id: this.selectedCategory,
    mensaje: '',
    image: '',
    created_at: new Date()
  };

  edit: boolean = false;

  constructor(private contactoService: ContactoService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getCategoria();
    const params = this.activatedRoute.snapshot.params;
    if (params.id) {
      this.contactoService.getOneContact(params.id)
        .subscribe(
          res => {
            console.log(res);
            this.contacto = res;
            console.log(this.contacto);
            this.edit = true;
          },
          err => console.log(err)
        )
    }
  }

  getCategoria() {
    this.contactoService.getCategoria().subscribe(
      res => console.log(
        this.categoria = res
      ),
      err => console.log(err)
    );
  }

  saveNewContact() {
    delete this.contacto.created_at;
    delete this.contacto.id;
    this.contactoService.saveContact(this.contacto)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/contacto']);
        },
        err => console.error(err)
      )
  }

  updateContact() {
    delete this.contacto.created_at;
    this.contactoService.updateContact(Number(this.contacto.id), this.contacto)
      .subscribe(
        res => { 
          console.log(res);
          this.router.navigate(['/contacto']);
        },
        err => console.error(err)
      )
  }

}

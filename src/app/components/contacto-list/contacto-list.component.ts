import { Component, HostBinding, OnInit } from '@angular/core';

import { ContactoService } from '../../services/contacto.service';
import { Contacto } from 'src/app/models/contacto';

@Component({
  selector: 'app-contacto-list',
  templateUrl: './contacto-list.component.html',
  styleUrls: ['./contacto-list.component.css']
})

export class ContactoListComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  contacto: any = [];

  constructor(private contactoService: ContactoService) { }

  ngOnInit() {
    this.getContact();
  }

  getContact() {
    this.contactoService.getContacto()
    .subscribe(
      res => (
        this.contacto = res
      ),
      err => console.log(err)
    );
  }

  deleteContacto(id: string){
    this.contactoService.deleteContact(id).subscribe(
    res => {
      console.log(res);
      this.getContact();
    },
    err => console.log(err)
    )
  }
  

}

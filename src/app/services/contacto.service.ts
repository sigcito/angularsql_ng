import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Contacto } from '../models/contacto';
import { Categoria } from '../models/categoria';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  API_URI = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  getContacto() {
    return this.http.get(`${this.API_URI}/contacto`);
  }

  getOneContact(id: string){
    return this.http.get(`${this.API_URI}/contacto/edit/${id}`);
  }

  getCategoria(){
    return this.http.get(`${this.API_URI}/contacto/add`);
  }

  getCategoriaE(){
    return this.http.get(`${this.API_URI}/contacto//edit`);
  }

  saveContact(contacto: Contacto) {
    return this.http.post(`${this.API_URI}/contacto`, contacto);
  }

  deleteContact(id: string){
    return this.http.delete(`${this.API_URI}/contacto/${id}`);
  }

  updateContact(id: string|number, updatedContacto: Contacto): Observable<Contacto>{
    return this.http.put(`${this.API_URI}/contacto/${id}`, updatedContacto);
  }
}
